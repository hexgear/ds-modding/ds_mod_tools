#!/bin/sh
# Format C++ code using Clang Format
find . -iname *.h -o -iname *.cpp -o -iname *.hpp | xargs clang-format -i
