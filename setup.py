from setuptools import setup, find_packages

VERSION = '0.0.1'

setup(name='ds_mod_tools',
      version=VERSION,
      description="Don't Starve Mod Tools",
      long_description=open("README.md").read(),
      url='https://gitlab.com/hexgear/ds-modding/ds_mod_tools',
      license='MIT',
      author='HexGear Studio',
      author_email='hexgear@gmx.us',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False
)
