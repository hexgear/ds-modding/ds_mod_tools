# Contributing

Please take a look at the [HexGear Studio Contributing Guide](https://gitlab.com/hexgear/hexgear.gitlab.io/-/blob/develop/CONTRIBUTING.md#contributing-guide).
