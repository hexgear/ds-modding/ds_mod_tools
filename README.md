# Don't Starve Mod Tools

_This is a fork of the 'Don't Starve Mod Tools' available on Steam Workshop._

## Dependencies

- [ktools](https://gitlab.com/hexgear/ds-modding/ktools)

- [pillow](https://python-pillow.org)

- [premake](https://premake.github.io)

## Compiling

```sh
./configure
make
```

## Installation

Provided that Don't Starve has been installed via Steam:

```
ls ~/.local/share/Steam/steamapps/common/dont_starve/mods/
INSTALLING_MODS.txt  MAKING_MODS.txt modsettings.lua  screecher
```

The `autocompiler` assumes the following directory structure:

```
~/.local/share/Steam/steamapps/common/ds_mod_tools
```

## Usage

### GitLab CI/CD Pipeline Template

```yml
include:
  - remote: 'https://gitlab.com/hexgear/ds-modding/ds_mod_tools/-/raw/v0.0.1/.gitlab-ci-template.yml'
```

## Acknowledgement

Forked from Klei Entertainment's [ds_mod_tools](https://github.com/kleientertainment/ds_mod_tools).

## License

### Source Code

SPDX-License-Identifier: [MIT](https://spdx.org/licenses/MIT.html)

### Icon

The [tools icon](https://dontstarve.gamepedia.com/File:Icon_Tools.png) is the property of Klei Entertainment.

## See Also

- [Installing Spriter r10 on Ubuntu 20.04 LTS](https://gist.github.com/oxr463/dce55b2a7e364f42ebb92891dc31ef7e)
