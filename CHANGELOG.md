# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.1] - 2021-05-29

### Added
- Contributing guide.
- Changelog.
- Dockerfile and compose file.
- GitLab CI/CD pipeline to build docker image.
- GitLab CI/CD pipeline template for mods.
- Setuptools for Python packages.
- Usage instructions for tools.
- Documentation for autocompiler behavior regarding directory structure.
- Link to Spriter gist.

### Changed
- Ignore compiled Python bytecode (*.pyc) files.
- Flatten Python directory structure.
- Re-organize C++ code.
- Format C++ code using Clang Format.
- Simplify build steps.

### Removed
- Reference to wand.zip file.

[Unreleased]: https://gitlab.com/hexgear/ds-modding/ds_mod_tools/-/compare/master...develop
[0.0.1]: https://gitlab.com/hexgear/ds-modding/ds_mod_tools/-/releases/v0.0.1
