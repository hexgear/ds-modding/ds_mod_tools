# Build ktools
# https://github.com/nsimplex/ktools/pull/14
FROM gcc:8-buster as build

RUN apt update && \
    apt install -y \
      cmake \
      libmagick++-dev

WORKDIR /usr/src

RUN curl -Lo ktools.tar.gz \
      https://gitlab.com/hexgear/ds-modding/ktools/-/archive/4.4.0/ktools-4.4.0.tar.gz && \
    tar -xf ktools.tar.gz && \
    rm ktools.tar.gz

WORKDIR /usr/src/ktools-4.4.0

# Small fix of the KTools::inverseOf() return type
# https://github.com/nsimplex/ktools/pull/13
COPY ./patches/fix-inverseOf.patch .
RUN patch -p1 < ./fix-inverseOf.patch

RUN ./configure && \
    make && \
    make install

WORKDIR /usr/src

RUN curl -Lo premake.zip \
      https://sourceforge.net/projects/premake/files/Premake/4.4/premake-4.4-beta5-src.zip && \
    unzip premake.zip && \
    rm premake.zip

WORKDIR /usr/src/premake-4.4-beta5

RUN cd build/gmake.unix && \
    make && \
    cd ../.. && \
    cp bin/release/premake4 /usr/local/bin/

WORKDIR /usr/src/ds_mod_tools
COPY . /usr/src/ds_mod_tools

RUN ./configure && \
    make && \
    cp -R build/linux/mod_tools /opt/ds_mod_tools

FROM debian:buster-slim

RUN apt update && \
    apt install -y python-pip

WORKDIR /usr/src/ds_mod_tools
COPY . /usr/src/ds_mod_tools

COPY --from=build /opt/ds_mod_tools /opt/ds_mod_tools
COPY --from=build /usr/local/bin/krane /usr/local/bin/
COPY --from=build /usr/local/bin/ktech /usr/local/bin/

RUN apt update && \
    apt install -y \
      libmagick++-6.q16-8 \
      unzip \
      zip && \
    pip install -r requirements.txt && \
    python setup.py install && \
    cp ds_mod_tools/* /opt/ds_mod_tools/

WORKDIR /opt/ds_mod_tools
ENV PATH "${PATH}:/opt/ds_mod_tools"

